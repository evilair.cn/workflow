# workflow

#### 介绍
简单的流程图，前端绘制，有点击和突出显示功能。

使用snap.svg生成flowable工作流流程图


# 使用
引入脚本文件
```
<script src="snap.svg.js"></script>
<script src="diagram.js"></script>
```

#### 学习建议
- 参照 code/评卷流程.html进行设计自己的流程图
- 由于对水平线的判断使用了简单的位置判断，所以建议使用表格布局流程单元的位置
- 添加了单击事件，如果需要双击，鼠标移动等事件可自行学习svg画图添加。
- [snap.js简单教程](https://www.cnblogs.com/miid/p/5289123.html)

#### 存在问题
存在点击事件的时候，由于文字绘制在上层，点击事件加在了流程单元的框上了，需要
鼠标变成箭头才有点击效果。

#### 效果
![multisources](img/demo2.png)



